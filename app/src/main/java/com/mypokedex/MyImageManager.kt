package com.mypokedex

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class MyImageManager {

    fun loadImage(context: Context, media: String, photoImageView: ImageView){
        Glide.with(context).load(media).centerCrop()
            .placeholder(R.drawable.pokeball)
            .error(R.drawable.pokeball_no_color)
            .fallback(R.drawable.pokeball_no_color)
            .diskCacheStrategy(DiskCacheStrategy.ALL).into(photoImageView)
    }
}