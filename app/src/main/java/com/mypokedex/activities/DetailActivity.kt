package com.mypokedex.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.google.android.material.tabs.TabLayoutMediator
import com.mypokedex.Constants
import com.mypokedex.PokUtils
import com.mypokedex.R
import com.mypokedex.adapter.ViewPagerAdapter
import com.mypokedex.models.*
import com.mypokedex.responses.*
import com.mypokedex.service.PokeApiService
import kotlinx.android.synthetic.main.activity_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class DetailActivity : AppCompatActivity() {

    private val adapter by lazy { ViewPagerAdapter(this) }

    var list: ArrayList<Pokemon> = ArrayList()
    var currentPosition: Int = -1
    private lateinit var service : PokeApiService
    private lateinit var retrofit: Retrofit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        pager.adapter = adapter
        val tabLayoutMediator = TabLayoutMediator(tab_layout, pager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                when(position + 1){
                    1 -> {
                        tab.text = "Info 1"
                        /*tab.setIcon(R.drawable.ic_beach_access_black_24dp)
                        val badge:BadgeDrawable = tab.orCreateBadge
                        badge.backgroundColor = ContextCompat.getColor(applicationContext, R.color.colorAccent)
                        badge.isVisible = true*/
                    }
                    2 -> {
                        tab.text = "Info 2"
                        /*tab.setIcon(R.drawable.ic_bookmark_black_24dp)
                        val badge:BadgeDrawable = tab.orCreateBadge
                        badge.backgroundColor = ContextCompat.getColor(applicationContext, R.color.colorAccent)
                        badge.number = 10
                        badge.isVisible = true*/
                    }
                    3 -> {
                        tab.text = "Info 3"
                        /*tab.setIcon(R.drawable.ic_camera_alt_black_24dp)
                        val badge:BadgeDrawable = tab.orCreateBadge
                        badge.backgroundColor = ContextCompat.getColor(applicationContext, R.color.colorAccent)
                        badge.number = 100
                        badge.maxCharacterCount = 3
                        badge.isVisible = true*/
                    }
                }
            })
        tabLayoutMediator.attach()

        setUpRetrofit()
        val tempList: ArrayList<Pokemon>? = intent.getParcelableArrayListExtra(Constants().listPokemon)
        currentPosition = intent.getIntExtra(Constants().index, -1)
        if(tempList != null && currentPosition != -1 && currentPosition < tempList.size){
            list = tempList
            loadUI()
        }
    }

    private fun setUpRetrofit() {
        retrofit = Retrofit.Builder().baseUrl(Constants().baseUrl)
            .addConverterFactory(GsonConverterFactory.create()).build()
        service = retrofit.create(PokeApiService::class.java)
    }

    private fun loadUI() {
        adapter.clearViews()

        val pokemonResponseCall : Call<PokemonDetailsResponse> = service.getPokemon(currentPosition + 1)
        pokemonResponseCall.enqueue(createPokemonCallback())
        val pokemonSpeciesResponseCall : Call<PokemonSpeciesDetailsResponse> = service.getPokemonSpecies(currentPosition + 1)
        pokemonSpeciesResponseCall.enqueue(createSpeciesCallback())

        val currentPok: Pokemon = list[currentPosition]
        val prevPok: Pokemon? = PokUtils().getPrevPokemon(currentPosition, list)
        val nextPok: Pokemon? = PokUtils().getNextPokemon(currentPosition, list)

        val number: Int = currentPok.number
        setUpActionBar(currentPok, number)

        adapter.updateImage(number)

        loadButton(prevPok, buttonPrevPok)
        loadButton(nextPok, buttonNextPok)
    }

    private fun setUpActionBar(currentPok: Pokemon, number: Int) {
        val name: String? = currentPok.name
        if (name != null) supportActionBar?.title = name.toUpperCase(Locale.ROOT)
        supportActionBar?.subtitle = resources.getString(R.string.detail_numberN) + number.toString()
    }

    private fun loadButton(prevPok: Pokemon?, button: Button) {
        button.text = "..."
        if(prevPok != null){
            val prevText = prevPok.name + "  N.°" + prevPok.number
            button.text = prevText
        }
    }

    private fun createPokemonCallback(): Callback<PokemonDetailsResponse> {
        return object : Callback<PokemonDetailsResponse> {
            override fun onResponse(call: Call<PokemonDetailsResponse>, response: Response<PokemonDetailsResponse>) {
                if(response.isSuccessful){
                    val detailResponse: PokemonDetailsResponse = response.body()!!
                    val pokemonDetails = PokUtils().createPokemonDetails(detailResponse)
                    loadPokemonDetailsOnUI(pokemonDetails)
                }else { Log.e(Constants().tag, " onResponse not Succesful: " + response.errorBody()) }
            }
            override fun onFailure(call: Call<PokemonDetailsResponse>, t: Throwable) { Log.e(Constants().tag, " onFailure: " + t.message) }
        }
    }

    private fun loadPokemonDetailsOnUI(pokemonDetails: PokemonDetails) {
        adapter.updateHeightWeightAbilities(pokemonDetails)
        adapter.updateTypesStatsMoves(pokemonDetails)
    }

    private fun createSpeciesCallback(): Callback<PokemonSpeciesDetailsResponse> {
        return object : Callback<PokemonSpeciesDetailsResponse> {
            override fun onResponse(call: Call<PokemonSpeciesDetailsResponse>, response: Response<PokemonSpeciesDetailsResponse>) {
                if(response.isSuccessful){
                    val detailResponse: PokemonSpeciesDetailsResponse = response.body()!!
                    val pokemonSpeciesDetails = PokUtils().createPokemonSpecies(detailResponse)

                    requestEvolutionChain(detailResponse.evolutionChainReference.url)
                    adapter.updateEntryCategory(pokemonSpeciesDetails)
                }else { Log.e(Constants().tag, " onResponse not Succesful: " + response.errorBody()) }
            }
            override fun onFailure(call: Call<PokemonSpeciesDetailsResponse>, t: Throwable) { Log.e(Constants().tag, " onFailure: " + t.message) }
        }
    }

    private fun requestEvolutionChain(url: String) {
        val id: Int = PokUtils().getUrlId(url)
        if(id != -1){
            val evolutionChainResponseCall : Call<EvolutionChainResponse> = service.getEvolutionChain(id)
            evolutionChainResponseCall.enqueue(createEvolutionChainCallback())
        }
    }

    private fun createEvolutionChainCallback(): Callback<EvolutionChainResponse> {
        return object : Callback<EvolutionChainResponse> {
            override fun onResponse(call: Call<EvolutionChainResponse>, response: Response<EvolutionChainResponse>) {
                if(response.isSuccessful){
                    val detailResponse: EvolutionChainResponse = response.body()!!
                    adapter.updateEvolution(detailResponse.chain)
                }else { Log.e(Constants().tag, " onResponse not Succesful: " + response.errorBody()) }
            }
            override fun onFailure(call: Call<EvolutionChainResponse>, t: Throwable) { Log.e(Constants().tag, " onFailure: " + t.message) }
        }
    }

    //Buttons Actions

    fun previous(view: View){
        if(currentPosition < 1) return
        currentPosition--
        loadUI()
    }

    fun next(view: View){
        if(currentPosition == list.size - 1) return
        currentPosition++
        loadUI()
    }
}