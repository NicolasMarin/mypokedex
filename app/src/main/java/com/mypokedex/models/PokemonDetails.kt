package com.mypokedex.models

import com.mypokedex.responses.PokemonAbility
import com.mypokedex.responses.PokemonMove
import com.mypokedex.responses.PokemonStat
import com.mypokedex.responses.PokemonType

data class PokemonDetails(var height: Int, var weight: Int,
                          var types: ArrayList<PokemonType>,
                          var abilities: ArrayList<PokemonAbility>,
                          var stats: HashMap<String, Int>,
                          var moves: ArrayList<PokemonMove>)