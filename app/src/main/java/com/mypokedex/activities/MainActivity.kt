package com.mypokedex.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mypokedex.Constants
import com.mypokedex.R
import com.mypokedex.adapter.PokemonListAdapter
import com.mypokedex.models.Pokemon
import com.mypokedex.responses.PokemonResponse
import com.mypokedex.service.PokeApiService
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(){

    private lateinit var retrofit: Retrofit
    private lateinit var viewAdapter: PokemonListAdapter
    private lateinit var viewManager: GridLayoutManager

    private var validToLoad: Boolean = true
    private var offset: Int = 0
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewAdapter = PokemonListAdapter(this, createItemClickListener())
        viewManager = GridLayoutManager(this, 3)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        recyclerView.addOnScrollListener(createScrollListener())

        retrofit = Retrofit.Builder().baseUrl("https://pokeapi.co/api/v2/")
            .addConverterFactory(GsonConverterFactory.create()).build()

        validToLoad = true
        offset = 0
        populatePokemonList(offset)
    }

    private fun createItemClickListener(): PokemonListAdapter.OnItemClickListener {
        return object : PokemonListAdapter.OnItemClickListener{
            override fun onItemClicked(currentPosition: Int){
                val intent = Intent(applicationContext, DetailActivity::class.java).apply {
                    putExtra(Constants().index, currentPosition)
                    putExtra(Constants().listPokemon, viewAdapter.pokemonList)
                }
                startActivity(intent)
            }
        }
    }

    private fun createScrollListener(): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(dy > 0){
                    val visibleItemCount: Int = viewManager.childCount
                    val totalItemCount: Int = viewManager.itemCount
                    val pastVisibleItems: Int = viewManager.findFirstVisibleItemPosition()

                    if(validToLoad && (visibleItemCount + pastVisibleItems) >= totalItemCount){
                        validToLoad = false
                        offset += 20
                        populatePokemonList(offset)
                    }
                }
            }
        }
    }

    private fun populatePokemonList(offset: Int) {
        val service : PokeApiService = retrofit.create(PokeApiService::class.java)
        val pokemonResponseCall : Call<PokemonResponse> = service.getPokemonList(20, offset)
        pokemonResponseCall.enqueue(createCallback())
    }

    private fun createCallback(): Callback<PokemonResponse> {
        return object : Callback<PokemonResponse>{
            override fun onResponse(call: Call<PokemonResponse>, response: Response<PokemonResponse>) {
                validToLoad = true
                if(response.isSuccessful){
                    val pokemonResponse: PokemonResponse = response.body()!!
                    val pokemonList: ArrayList<Pokemon> = updateNumber(pokemonResponse.results, offset)
                    viewAdapter.addNewPokemonToList(pokemonList)
                }else { Log.e(Constants().tag, " onResponse: " + response.errorBody()) }
            }
            override fun onFailure(call: Call<PokemonResponse>, t: Throwable) {
                validToLoad = true
                Log.e(Constants().tag, " onFailure: " + t.message)
            }
        }
    }

    private fun updateNumber(results: ArrayList<Pokemon>, offset: Int): ArrayList<Pokemon> {
        var cont: Int = offset
        val resultsUpdated = ArrayList<Pokemon>()
        for(pokemon: Pokemon in results){
            cont += 1
            pokemon.number = cont
            resultsUpdated.add(pokemon)
        }
        return resultsUpdated
    }
}