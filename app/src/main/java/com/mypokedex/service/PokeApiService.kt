package com.mypokedex.service

import com.mypokedex.responses.EvolutionChainResponse
import com.mypokedex.responses.PokemonDetailsResponse
import com.mypokedex.responses.PokemonResponse
import com.mypokedex.responses.PokemonSpeciesDetailsResponse
import retrofit2.*
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokeApiService {

    @GET("pokemon-species")
    fun getPokemonList(@Query("limit") limit: Int,
                       @Query("offset") offset: Int) : Call<PokemonResponse>

    @GET("pokemon/{number}/")
    fun getPokemon(@Path("number") number: Int) : Call<PokemonDetailsResponse>

    @GET("pokemon-species/{number}")
    fun getPokemonSpecies(@Path("number") number: Int) : Call<PokemonSpeciesDetailsResponse>

    @GET("evolution-chain/{number}/")
    fun getEvolutionChain(@Path("number") number: Int) : Call<EvolutionChainResponse>
}