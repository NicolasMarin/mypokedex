package com.mypokedex

class Constants {

    val tag = "LogPok"
    val baseUrl = "https://pokeapi.co/api/v2/"
    val index = "index"
    val imageUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
    val listPokemon = "listPok"


    val IMAGE_NUMBER = "IMAGE_NUMBER"
    val ENTRY_VALUE = "ENTRY_VALUE"
    val CATEGORY_VALUE = "CATEGORY_VALUE"
    val WEIGHT_VALUE = "WEIGHT_VALUE"
    val HEIGHT_VALUE = "HEIGHT_VALUE"
    val ABILITIES_VALUE = "ABILITIES"
    val TYPES_VALUE = "TYPES_VALUE"
    val STATS_VALUE = "STATS_VALUE"
    val MOVES_VALUE = "MOVES_VALUE"
    val EVOLUTION_VALUES = "EVOLUTION_VALUES"


}