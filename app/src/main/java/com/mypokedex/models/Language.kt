package com.mypokedex.models

data class Language(var name: String)
