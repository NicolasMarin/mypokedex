package com.mypokedex.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.mypokedex.Constants
import com.mypokedex.PokUtils
import com.mypokedex.fragments.TabFragmentOne
import com.mypokedex.fragments.TabFragmentThree
import com.mypokedex.fragments.TabFragmentTwo
import com.mypokedex.models.PokemonDetails
import com.mypokedex.models.PokemonSpeciesDetails
import com.mypokedex.responses.EvolutionChain
import com.mypokedex.responses.PokemonMove
import com.mypokedex.responses.PokemonType

class ViewPagerAdapter(fa: FragmentActivity): FragmentStateAdapter(fa) {

    private val fragmentList = listOf(TabFragmentOne(), TabFragmentTwo(), TabFragmentThree())
    private var imageNumber = -1
    private var entryValue = ""
    private var categoryValue = ""
    private var heightValue = ""
    private var weightValue = ""
    private var abilitiesValue = ""
    private var types = ArrayList<PokemonType>()
    private var stats = HashMap<String, Int>()
    private var moves = ArrayList<PokemonMove>()
    private var evolutionChain: EvolutionChain? = null

    companion object{ private const val ARG_OBJECT = "object" }
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        val bundle = when (position){
            0 -> loadBundleOne(Bundle())
            1 -> loadBundleTwo(Bundle())
            2 -> loadBundleThree(Bundle())
            else -> Bundle()
        }
        val frag = when(position){
            0 -> fragmentList[0]
            1 -> fragmentList[1]
            2 -> fragmentList[2]
            else -> fragmentList[0]
        }
        frag.arguments = bundle
        return frag
    }

    private fun loadBundleOne(bundle: Bundle): Bundle {
        bundle.putInt(Constants().IMAGE_NUMBER, imageNumber)
        bundle.putString(Constants().ENTRY_VALUE, entryValue)
        bundle.putString(Constants().CATEGORY_VALUE, categoryValue)

        bundle.putString(Constants().HEIGHT_VALUE, heightValue)
        bundle.putString(Constants().WEIGHT_VALUE, weightValue)
        bundle.putString(Constants().ABILITIES_VALUE, abilitiesValue)
        return bundle
    }

    private fun loadBundleTwo(bundle: Bundle): Bundle {
        bundle.putParcelableArrayList(Constants().TYPES_VALUE, types)
        bundle.putSerializable(Constants().STATS_VALUE, stats)
        bundle.putParcelableArrayList(Constants().MOVES_VALUE, moves)
        return bundle
    }

    private fun loadBundleThree(bundle: Bundle): Bundle {
        bundle.putParcelable(Constants().EVOLUTION_VALUES, evolutionChain)
        return bundle
    }

    fun updateImage(number: Int) {
        imageNumber = number
        createFragment(0)
    }

    fun updateEntryCategory(speciesDetails: PokemonSpeciesDetails) {
        entryValue = speciesDetails.entry!!.flavor_text.replace("\n", " ")
        categoryValue = speciesDetails.category!!.categoryName
        updateFragment(0)
    }

    private fun updateFragment(position: Int) {
        createFragment(position)
        fragmentList[position].updateView()
    }

    fun updateHeightWeightAbilities(pokemonDetails: PokemonDetails) {
        heightValue = PokUtils().convertNumber(pokemonDetails.height.toString()) + " m"
        weightValue = PokUtils().convertNumber(pokemonDetails.weight.toString()) + " kg"
        abilitiesValue = PokUtils().getAbilitiesToShow(pokemonDetails.abilities)
        updateFragment(0)
    }

    fun updateTypesStatsMoves(pokemonDetails: PokemonDetails) {
        types = pokemonDetails.types
        stats = pokemonDetails.stats
        moves = pokemonDetails.moves
        updateFragment(1)
    }

    fun updateEvolution(detailResponse: EvolutionChain) {
        evolutionChain = detailResponse
        updateFragment(2)
    }

    fun clearViews() {
        for(tabFragment in fragmentList){
            tabFragment.clearViews()
        }
    }
}