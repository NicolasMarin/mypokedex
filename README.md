# MyPokedex [![build status](https://gitlab.com/NicolasMarin/mypokedex/badges/master/pipeline.svg?job=assembleDebug&key_text=Build)](https://gitlab.com/NicolasMarin/mypokedex/-/commits/master)

An Android Application, programmed in Kotlin, that recreates a Pokedex from the Pokemon anime, consuming the Api-Rest PokéAPI.


## Screenshots

<p float="left">
  <img src="images/Screenshot_01.png" width="350" alt="Screenshot de la Aplicacion"/>

  <img src="images/Screenshot_02.png" width="350" alt="Screenshot de la Aplicacion"/>

  <img src="images/Screenshot_03.png" width="350" alt="Screenshot de la Aplicacion"/>
</p>


## Built with

* [Retrofit](https://square.github.io/retrofit/) - Library used to make requests to the Api-Rest PokéAPI.
* [Gson](https://github.com/google/gson) - Library used to convert the responses in Json format of the requests to the Api-Rest, to Java/Kotlin format.
* [Glide](https://github.com/bumptech/glide) - Library used to load images.
* [RecyclerView](https://developer.android.com/reference/androidx/recyclerview/widget/RecyclerView) - Class used as View in which to list the Pokemon obtained as a response to the request to the API.
