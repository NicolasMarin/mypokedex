package com.mypokedex.fragments

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.mypokedex.*
import com.mypokedex.responses.ChainElement
import com.mypokedex.responses.EvolutionChain
import kotlinx.android.synthetic.main.fragment_tab_three.*

class TabFragmentThree : TabFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_three, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateView()
    }

    override fun updateView() { loadEvolutionChain() }

    override fun clearViews() {
        if(layoutPokEvolutions != null) layoutPokEvolutions.removeAllViews()
    }

    private fun loadEvolutionChain() {
        if(layoutPokEvolutions != null) layoutPokEvolutions.removeAllViews()
        var evolutionChain: EvolutionChain? = null
        arguments?.getParcelable<EvolutionChain>(Constants().EVOLUTION_VALUES)?.let { evolutionChain = it }
        loadEvolutionStage(getLevelList(evolutionChain))
    }

    private fun getLevelList(evolutionChain: EvolutionChain?): ArrayList<ArrayList<ChainElement>> {
        var levelList = ArrayList<ArrayList<ChainElement>>()
        if(evolutionChain != null){
            levelList = loadALevel(evolutionChain, 0, levelList)
        }
        return levelList
    }

    private fun loadALevel(evolutionChain: EvolutionChain, index: Int, levelList: ArrayList<ArrayList<ChainElement>>): ArrayList<ArrayList<ChainElement>> {
        var myLevelList = levelList
        var myIndex = index

        while (myLevelList.size <= myIndex){
            myLevelList.add(ArrayList())
        }
        myLevelList[myIndex].add(evolutionChain.partOfChain)

        if(evolutionChain.evolvesTo.isNotEmpty()){
            myIndex++
            for(evolvesTo in evolutionChain.evolvesTo){
                myLevelList = loadALevel(evolvesTo, myIndex, myLevelList)
            }
        }
        return myLevelList
    }

    private fun loadEvolutionStage(listOfLevels: ArrayList<ArrayList<ChainElement>>) {
        var layoutId = R.layout.evolution_stage
        for(index in 0 until listOfLevels.size){
            layoutId = loadLevelOnView(listOfLevels[index], index, listOfLevels, layoutId)
        }
    }

    private fun loadLevelOnView(levelList: ArrayList<ChainElement>, index: Int,
        listOfLevels: ArrayList<ArrayList<ChainElement>>, layoutId: Int): Int {
        var currentLayoutId = layoutId
        if(activity != null && layoutPokEvolutions != null){
            when(levelList.size){
                0 -> return currentLayoutId
                1 -> addChainElementViewToLayout(layoutPokEvolutions, levelList[0], index, levelList.size, listOfLevels, currentLayoutId)
                2 -> {
                    currentLayoutId = R.layout.evolution_stage_mid
                    addDoubleViewsToLayoutPokEvolutions(levelList, index, listOfLevels, currentLayoutId)
                }
                else -> {
                    currentLayoutId = R.layout.evolution_stage_mid
                    addMoreThanTwoViewsToLayoutPokEvolutions(levelList, index, listOfLevels, currentLayoutId)
                }
            }
        }
        return currentLayoutId
    }

    private fun addMoreThanTwoViewsToLayoutPokEvolutions(levelList: ArrayList<ChainElement>, index: Int, listOfLevels: ArrayList<ArrayList<ChainElement>>, layoutId: Int) {
        var linearHorizontal = LinearLayout(context)
        for(currentIndex in 0 until levelList.size){
            if(currentIndex % 2 == 0){
                linearHorizontal = createLinearLayout()
                layoutPokEvolutions.addView(linearHorizontal)
            }
            addChainElementViewToLayout(linearHorizontal, levelList[currentIndex], index, levelList.size, listOfLevels, layoutId)
        }
    }

    private fun addDoubleViewsToLayoutPokEvolutions(levelList: ArrayList<ChainElement>, index: Int,
                                                   listOfLevels: ArrayList<ArrayList<ChainElement>>, layoutId: Int) {
        val linearHorizontal = createLinearLayout()
        for(levelListItem in levelList){
            addChainElementViewToLayout(linearHorizontal, levelListItem, index, levelList.size, listOfLevels, layoutId)
        }
        layoutPokEvolutions.addView(linearHorizontal)
    }

    private fun createLinearLayout(): LinearLayout {
        val linearHorizontal = LinearLayout(context)
        linearHorizontal.orientation = LinearLayout.HORIZONTAL
        linearHorizontal.gravity = Gravity.CENTER_HORIZONTAL
        linearHorizontal.weightSum = 2F
        return linearHorizontal
    }

    private fun addChainElementViewToLayout(layout: LinearLayout?, chainElement: ChainElement, index: Int, levelListSize: Int,
                                            listOfLevels: ArrayList<ArrayList<ChainElement>>, layoutToInflateId: Int) {
        layout?.addView(getEvolutionStageView(chainElement, index, levelListSize,
            if(index + 1 < listOfLevels.size) listOfLevels[index+1].size else 0, layoutToInflateId))
    }

    private fun getEvolutionStageView(chainElement: ChainElement, index: Int, siblingsCant: Int, evolveToCant: Int, layoutId: Int): View {
        val evolutionStageView = LayoutInflater.from(context).inflate(layoutId, layoutPokEvolutions, false)
        val name = chainElement.name
        val textView = evolutionStageView.findViewById(R.id.tvName) as TextView
        textView.text = if(name.isNotEmpty()) (name[0].toUpperCase() + name.substring(1, name.length)) else name
        val url = PokUtils().getImageUrl(PokUtils().getUrlId(chainElement.url))
        val imageView = evolutionStageView.findViewById(R.id.imgEvolStage) as ImageView
        MyImageManager().loadImage(activity!!, url, imageView)

        if(index == 0 || siblingsCant > 2) disappearImage(evolutionStageView, R.id.imgArrow1)
        if(evolveToCant < 3) disappearImage(evolutionStageView, R.id.imgArrow2)

        return evolutionStageView
    }

    private fun disappearImage(evolutionStageView: View, imageViewId: Int) {
        val image = evolutionStageView.findViewById(imageViewId) as ImageView
        image.visibility = View.GONE
    }
}