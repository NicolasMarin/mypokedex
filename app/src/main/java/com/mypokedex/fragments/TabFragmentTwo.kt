package com.mypokedex.fragments

import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.mypokedex.Constants
import com.mypokedex.PokUtils
import com.mypokedex.R
import com.mypokedex.responses.PokemonMove
import com.mypokedex.responses.PokemonType
import kotlinx.android.synthetic.main.fragment_tab_three.*
import kotlinx.android.synthetic.main.fragment_tab_two.*
import kotlinx.android.synthetic.main.stats_table.*
import java.util.*
import kotlin.collections.HashMap

class TabFragmentTwo : TabFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_two, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateView()
    }

    override fun updateView() {
        loadTypes()
        loadStats()
        loadMoves()
    }

    override fun clearViews() {
        if(layoutPokTypes != null) layoutPokTypes.removeAllViews()
        if(layoutPokMoves != null) layoutPokMoves.removeAllViews()
    }

    private fun loadTypes() {
        var types = ArrayList<PokemonType>()
        arguments?.getParcelableArrayList<PokemonType>(Constants().TYPES_VALUE)?.let { types = it }
        if(activity != null && layoutPokTypes != null){
            layoutPokTypes.removeAllViews()
            for(i in 0..2){
                val tvType = TextView(activity)
                val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
                tvType.textSize = 20F
                if(i < types.size){
                    tvType.text = types[i].name!!.toUpperCase(Locale.ROOT)
                    tvType.setBackgroundResource(PokUtils().getBackgroundColorToType(types[i].name!!))
                    tvType.setTextColor(PokUtils().getTextColorToType(types[i].name!!))
                    tvType.gravity = Gravity.CENTER
                    val padding: Int = resources.getDimensionPixelSize(R.dimen.detail_type_padding)
                    tvType.setPadding(padding, padding, padding, padding)
                    params.setMargins(if(i == 0) 0 else padding, padding, if(i == 2) 0 else padding, padding)
                }
                layoutPokTypes.addView(tvType, params)
            }
        }
    }

    private fun loadStats() {
        val serializable = arguments?.getSerializable(Constants().STATS_VALUE)
        if(serializable != null){
            val stats = serializable as HashMap<String, Int>
            val idList = listOf(pokHP, pokSpeed, pokAttack, pokDefense, pokSpecialAttack, pokSpecialDefense)
            val keyList = listOf("hp", "speed", "attack", "defense", "special-attack", "special-defense")
            for(i in 0..5){
                if(idList[i] != null) idList[i].text = stats[keyList[i]].toString()
            }
        }
    }

    private fun loadMoves() {
        var moves = ArrayList<PokemonMove>()
        arguments?.getParcelableArrayList<PokemonMove>(Constants().MOVES_VALUE)?.let { moves = it }
        if(activity != null && layoutPokMoves != null){
            layoutPokMoves.removeAllViews()
            for(move in moves){
                val textView = TextView(activity)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    textView.setTextAppearance(R.style.DetailActivityBasicText)
                }
                val padding: Int = resources.getDimension(R.dimen.detail_type_padding).toInt()
                textView.setPadding(padding, padding, padding, padding)
                textView.text = "- " + move.name
                layoutPokMoves.addView(textView)
            }
        }
    }
}