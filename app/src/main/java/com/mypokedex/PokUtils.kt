package com.mypokedex

import android.graphics.Color
import com.mypokedex.models.Pokemon
import com.mypokedex.models.PokemonDetails
import com.mypokedex.models.PokemonSpeciesDetails
import com.mypokedex.responses.*
import java.util.*
import kotlin.collections.ArrayList

class PokUtils {

    fun getImageUrl(number: Int): String {
        val numberChecked: String =
            if(number < 10) "00$number" else (if (number < 100) "0$number" else number.toString())
        return "https://assets.pokemon.com/assets/cms2/img/pokedex/full/$numberChecked.png"
    }

    fun getNextPokemon(position: Int, list: ArrayList<Pokemon>): Pokemon?{
        return if(position == list.size - 1) null else list[position + 1]
    }

    fun getPrevPokemon(position: Int, list: ArrayList<Pokemon>): Pokemon?{
        return if(position < 1) null else list[position - 1]
    }

    /*
    7 -> 0,7 m
    69 -> 6,9 kg
    10 -> 1,0 m
    130 -> 13,0 kg
    20 -> 2,0 m
    1000 -> 100,0 kg
    */
    fun convertNumber(originalValue: String): String {
        if(originalValue.isEmpty()) return ""
        if(originalValue.length == 1) return "0,$originalValue"
        return originalValue.substring(0, originalValue.length - 1) + "," + originalValue[originalValue.length - 1]
    }

    fun getBackgroundColorToType(type: String): Int {
        when (type) {
            "grass" -> return R.color.colorGrass
            "poison" -> return R.color.colorPoison
            "fire" -> return R.color.colorFire
            "normal" -> return R.color.colorNormal
            "water" -> return R.color.colorWater
            "electric" -> return R.color.colorElectric
            "ice" -> return R.color.colorIce
            "fighting" -> return R.color.colorFighting
            "psychic" -> return R.color.colorPsychic
            "bug" -> return R.color.colorBug
            "rock" -> return R.color.colorRock
            "ghost" -> return R.color.colorGhost
            "dark" -> return R.color.colorDark
            "steel" -> return R.color.colorSteel
            "fairy" -> return R.color.colorFairy
            "ground" -> return R.color.colorGround
            "flying" -> return R.color.colorFlying
            "dragon" -> return R.color.colorDragon
            else -> return R.color.colorPrimary
        }
    }

    fun getTextColorToType(type: String): Int {
        return if(type == "normal") Color.BLACK else Color.WHITE
    }

    fun getAbilitiesToShow(abilities: ArrayList<PokemonAbility>): String {
        var abilitiesText = ""
        for(i in 0 until abilities.size){
            abilitiesText += abilities[i].name.toUpperCase(Locale.ROOT)
            if(i < abilities.size-1) abilitiesText += "\n"
        }
        return abilitiesText
    }

    fun getPokemonEntry(entries: ArrayList<PokemonEntry>): PokemonEntry? {
        val language: String = Locale.getDefault().language
        for(i in entries.size-1 downTo 0){
            if(entries[i].language.name == language) return entries[i]
        }
        return null
    }

    fun getPokemonCategory(categories: ArrayList<PokemonCategory>): PokemonCategory? {
        val language: String = Locale.getDefault().language
        for(i in categories.size-1 downTo 0){
            if(categories[i].language.name == language) return categories[i]
        }
        return null
    }

    fun getUrlId(url: String): Int {
        val cutUrl = if(url[url.length-1] == '/') url.substring(0, url.length-1) else url
        val slashBeforeId = cutUrl.lastIndexOf('/')
        if(slashBeforeId != -1){
            return cutUrl.substring(slashBeforeId + 1, cutUrl.length).toInt()
        }
        return -1
    }

    private fun getMoves(moves: ArrayList<MovesFromAPokemon>): ArrayList<PokemonMove> {
        val pokemonMoves = ArrayList<PokemonMove>()
        for(moveFrom in moves){
            pokemonMoves.add(moveFrom.move)
        }
        return pokemonMoves
    }

    private fun getStats(stats: ArrayList<StatsFromAPokemon>): HashMap<String, Int> {
        val statsMap = HashMap<String, Int>()
        for(statFrom in stats){
            statsMap[statFrom.stat.name] = statFrom.base_stat
        }
        return statsMap
    }

    private fun getAbilities(abilities: ArrayList<AbilitiesFromAPokemon>): ArrayList<PokemonAbility> {
        val pokemonAbilities = ArrayList<PokemonAbility>()
        for(abilityFrom in abilities){
            pokemonAbilities.add(abilityFrom.ability)
        }
        return pokemonAbilities
    }

    private fun getTypes(types: ArrayList<TypeFromAPokemon>): ArrayList<PokemonType> {
        val pokemonTypes = ArrayList<PokemonType>()
        for(typeFrom in types){
            pokemonTypes.add(typeFrom.type)
        }
        return pokemonTypes
    }

    fun createPokemonDetails(response: PokemonDetailsResponse): PokemonDetails {
        return PokemonDetails(response.height, response.weight, getTypes(response.types),
            getAbilities(response.abilities), getStats(response.stats), getMoves(response.moves))
    }

    fun createPokemonSpecies(response: PokemonSpeciesDetailsResponse): PokemonSpeciesDetails {
        return PokemonSpeciesDetails(getPokemonEntry(response.entries), getPokemonCategory(response.categories))
    }

}