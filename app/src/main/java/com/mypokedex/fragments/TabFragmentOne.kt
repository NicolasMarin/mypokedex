package com.mypokedex.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mypokedex.Constants
import com.mypokedex.MyImageManager
import com.mypokedex.PokUtils
import com.mypokedex.R
import kotlinx.android.synthetic.main.fragment_tab_one.*
import kotlinx.android.synthetic.main.height_weight_detail.*

class TabFragmentOne : TabFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateView()
    }

    override fun updateView() {
        loadImage()
        loadEntryCategory()
        loadHeightWeightAbilities()
    }

    override fun clearViews() {
        clearTextView(pokEntry)
        clearTextView(pokCategory)
        clearTextView(pokHeight)
        clearTextView(pokWeight)
        clearTextView(pokAbilities)
    }

    private fun clearTextView(textView: TextView?) {
        if(textView != null) textView.text = ""
    }

    private fun loadImage() {
        var number = -1
        arguments?.getInt(Constants().IMAGE_NUMBER)?.let { number = it }
        if(number != -1 && pokSelectedImage != null){
            val url = PokUtils().getImageUrl(number)
            context?.let { MyImageManager().loadImage(it, url, pokSelectedImage) }
        }
    }

    private fun loadEntryCategory() {
        loadTextView(Constants().ENTRY_VALUE, pokEntry)
        loadTextView(Constants().CATEGORY_VALUE, pokCategory)
    }

    private fun loadHeightWeightAbilities() {
        loadTextView(Constants().HEIGHT_VALUE, pokHeight)
        loadTextView(Constants().WEIGHT_VALUE, pokWeight)
        loadTextView(Constants().ABILITIES_VALUE, pokAbilities)
    }

    private fun loadTextView(key: String, textView: TextView?) {
        var value = ""
        arguments?.getString(key)?.let { value = it }
        if(value.isNotEmpty() && textView != null) textView.text = value
    }

}