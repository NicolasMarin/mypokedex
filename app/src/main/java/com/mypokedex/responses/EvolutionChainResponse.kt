package com.mypokedex.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class EvolutionChainResponse(@SerializedName("chain") var chain: EvolutionChain)

class EvolutionChain(@SerializedName("evolves_to") var evolvesTo: ArrayList<EvolutionChain>,
                          @SerializedName("species") var partOfChain: ChainElement): Parcelable {
    constructor(parcel: Parcel) : this(TODO("evolvesTo"), TODO("partOfChain")) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {}

    override fun describeContents(): Int { return 0 }

    companion object CREATOR : Parcelable.Creator<EvolutionChain> {
        override fun createFromParcel(parcel: Parcel): EvolutionChain { return EvolutionChain(parcel) }

        override fun newArray(size: Int): Array<EvolutionChain?> { return arrayOfNulls(size) }
    }

    override fun toString(): String {
        return "partOfChain:$partOfChain, evolvesTo: $evolvesTo\n"
    }
}

data class ChainElement(@SerializedName("name") @Expose var name: String,
                        @SerializedName("url") @Expose var url: String)
