package com.mypokedex.fragments

import androidx.fragment.app.Fragment

abstract class TabFragment : Fragment() {

    abstract fun updateView()

    abstract fun clearViews()

}