package com.mypokedex.models

import com.mypokedex.responses.ChainElement

data class EvolutionChainDetails(var evolvesTo: ArrayList<EvolutionChainDetails>,
                                 var partOfChain: ChainElement)
