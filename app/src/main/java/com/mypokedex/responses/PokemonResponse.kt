package com.mypokedex.responses

import com.mypokedex.models.Pokemon

data class PokemonResponse(var results: ArrayList<Pokemon>)