package com.mypokedex.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mypokedex.Constants
import com.mypokedex.activities.MainActivity
import com.mypokedex.MyImageManager
import com.mypokedex.R
import com.mypokedex.models.Pokemon
import kotlinx.android.synthetic.main.item_pokemon.view.*

class PokemonListAdapter(var context: Context, private val itemClickListener: OnItemClickListener): RecyclerView.Adapter<PokemonListAdapter.PokemonViewHolder>() {
    val pokemonList = ArrayList<Pokemon>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        return PokemonViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        holder.bind(position, itemClickListener, context)
    }

    override fun getItemCount() = pokemonList.size

    fun addNewPokemonToList(newPokemonList: ArrayList<Pokemon>) {
        pokemonList.addAll(newPokemonList)
        notifyDataSetChanged()
    }

    inner class PokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private val photoImageView: ImageView = itemView.photoImageView
        private val nameTextView: TextView = itemView.nameTextView

        fun bind(position: Int, clickListener: OnItemClickListener, context: Context){
            val currentPok = pokemonList[position]
            nameTextView.text = currentPok.name
            val url = Constants().imageUrl + currentPok.number + ".png"
            MyImageManager().loadImage(context, url, photoImageView)
            itemView.setOnClickListener {
                clickListener.onItemClicked(position)//currentPok, prevPok, nextPok)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClicked(currentPosition: Int)
    }
}