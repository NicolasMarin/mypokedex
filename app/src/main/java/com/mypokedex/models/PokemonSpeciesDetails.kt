package com.mypokedex.models

import com.mypokedex.responses.PokemonCategory
import com.mypokedex.responses.PokemonEntry

data class PokemonSpeciesDetails(var entry: PokemonEntry?, var category: PokemonCategory?)