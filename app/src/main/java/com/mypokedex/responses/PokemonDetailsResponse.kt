package com.mypokedex.responses

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PokemonDetailsResponse(var height: Int, var weight: Int,
                                  @SerializedName("types") var types: ArrayList<TypeFromAPokemon>,
                                  @SerializedName("abilities") var abilities: ArrayList<AbilitiesFromAPokemon>,
                                  @SerializedName("stats") var stats: ArrayList<StatsFromAPokemon>,
                                  @SerializedName("moves") var moves: ArrayList<MovesFromAPokemon>)

data class TypeFromAPokemon(@SerializedName("slot") @Expose var slot: Int, @SerializedName("type") var type: PokemonType)
data class AbilitiesFromAPokemon(@SerializedName("is_hidden") @Expose var is_hidden: Boolean, @SerializedName("ability") var ability: PokemonAbility)
data class StatsFromAPokemon(@SerializedName("base_stat") @Expose var base_stat: Int, @SerializedName("stat") var stat: PokemonStat)
data class MovesFromAPokemon(@SerializedName("move") @Expose var move: PokemonMove)

class PokemonType(@SerializedName("name") @Expose var name: String?): Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString()) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) { parcel.writeString(name) }

    override fun describeContents(): Int { return 0 }

    companion object CREATOR : Parcelable.Creator<PokemonType> {
        override fun createFromParcel(parcel: Parcel): PokemonType { return PokemonType(parcel) }

        override fun newArray(size: Int): Array<PokemonType?> { return arrayOfNulls(size) }
    }
}

data class PokemonAbility(@SerializedName("name") @Expose var name: String)
data class PokemonStat(@SerializedName("name") @Expose var name: String)
class PokemonMove(@SerializedName("name") @Expose var name: String?): Parcelable{
    constructor(parcel: Parcel) : this(parcel.readString()) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) { parcel.writeString(name) }

    override fun describeContents(): Int { return 0 }

    companion object CREATOR : Parcelable.Creator<PokemonMove> {
        override fun createFromParcel(parcel: Parcel): PokemonMove { return PokemonMove(parcel) }
        override fun newArray(size: Int): Array<PokemonMove?> { return arrayOfNulls(size) }
    }

}
