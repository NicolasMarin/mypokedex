package com.mypokedex.responses

import com.google.gson.annotations.SerializedName
import com.mypokedex.models.Language

data class PokemonSpeciesDetailsResponse(@SerializedName("flavor_text_entries") var entries: ArrayList<PokemonEntry>,
                                         @SerializedName("genera") var categories: ArrayList<PokemonCategory>,
                                         @SerializedName("evolution_chain") var evolutionChainReference: EvolutionChainReference)

data class PokemonCategory(@SerializedName("genus") var categoryName: String,
                           @SerializedName("language") var language: Language)

data class PokemonEntry(@SerializedName("flavor_text") var flavor_text: String,
                        @SerializedName("language") var language: Language)

data class EvolutionChainReference(@SerializedName("url") var url: String)
